#include <AFMotor.h>

#define JOG_TIME 10
#define BRIGHTNESS_AV_COUNT 10 
#define BRIGHTNESS_AV_TIME 1 //ms
#define END_SW_AV_COUNT 10 
#define END_SW_AV_TIME 1 //ms
#define BRIGHTNESS_THRESH 300
#define BRIGHTNESS_HY 50
#define POST_SUNSET_TIME 60 //mins
#define POST_SUNRISE_TIME 0 //mins
#define BRIGHTNESS_FALSE_POS_ALLOW POST_SUNSET_TIME/10
#define REEL_IN_TIME 12 // TODO - tune
#define REEL_OUT_TIME 10  // TODO - tune
#define CIRC_COUNT 10

// Define daytime states
#define NIGHT 1
#define DAWN 2
#define DAY 3
#define DUSK 4

// Define door states
#define OPEN 0
#define CLOSED 1

// Define pinouts
int light_sensor = A8;
int end_sw = 42;
int jog_down_but = 47;
int jog_up_but = 50;
AF_DCMotor motor(1); // DC motor on M1

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
   
  // turn on motor #2
  motor.setSpeed(200);
  motor.run(RELEASE);

  pinMode(end_sw, INPUT);
  pinMode(jog_down_but, INPUT);
  pinMode(jog_up_but, INPUT);
  pinMode(13, OUTPUT);
  delay(100);
}

// Global variables
int state = DAY;
int door_state = OPEN;
int circular_count = 0;
int day_duration_mins = 0;
int night_duration_mins = 0;

// Function prototypes
void jog_motor_down(void);
void jog_motor_up(void);
void stop_motor(void);
int brightness(void);
int end_switch_status(void);
void delay_mins(int);
void ensure_door_closed(void);
void ensure_door_open(void);
int dawn_coming(void);
int dusk_coming(void);
void print_status(void);

void loop() {
  print_status();
  switch (state) {
    case NIGHT:  
      // Clear duration timer for day-length reasonableness check
      day_duration_mins = -1;
      // Door should be shut
      ensure_door_closed();
      // At first hint of light, change state
      if (dawn_coming()) {
        state = DAWN;
      }
      break;
    case DAWN:
      // Checking to ensure false dawn not detected
      break;
    case DAY:
      // Clear duration timer for night-length reasonableness check
      night_duration_mins = -1;
      // Door should be open
      ensure_door_open();
      // At first hint of darkness, change state
      if (dusk_coming()) {
        state = DUSK;
      }
      break;
    case DUSK:
      // Checking to ensure false dusk not detected
      break;
    default:
      Serial.println("Entered undefined state");
      state = NIGHT;
      break;
  }
  delay(500);
}

void ensure_door_closed(void) {
  if (door_state == CLOSED) {
    return;
  } else {
    // Close door
    int start_time = millis();
    while ( (end_switch_status() == 1) & ((millis() - start_time) < REEL_IN_TIME)) {
      jog_motor_up();
    }
    stop_motor();
    // Mark door as closed
    door_state = CLOSED;
  }
}

void ensure_door_open(void) {
  if (door_state == OPEN) {
    return;
  } else {
    // Open door
    int start_time = millis();
    while ((millis() - start_time) < REEL_OUT_TIME) {
      jog_motor_down();
    }
    stop_motor();
    // Mark door as closed
    door_state = OPEN;
  }
}

int dawn_coming(void) {
  if (brightness() > (BRIGHTNESS_THRESH+BRIGHTNESS_HY)) {
    return 1;
  }
  return 0;
}

int dusk_coming(void) {
 if (brightness() < (BRIGHTNESS_THRESH-BRIGHTNESS_HY)) {
    return 1;
  }
  return 0;
}

void print_status(void) {
  Serial.print("state = ");
  switch (state) {
    case NIGHT:
      Serial.println("NIGHT");
      break;
    case DAWN:
      Serial.println("DAWN");
      break;
    case DAY:
      Serial.println("DAY");
      break;
    case DUSK:
      Serial.println("DUSK");
      break;
  }
  Serial.print("door_state = ");
  switch (door_state) {
    case OPEN:
      Serial.println("OPEN");
      break;
    case CLOSED:
      Serial.println("CLOSED");
      break;
  }
  Serial.println(door_state);
  Serial.print("circular_count = ");
  Serial.println(circular_count);
  circular_count++;
  if (circular_count > CIRC_COUNT) {
    circular_count = 0;
  }
}

void code_dump() {
  /*
  if (digitalRead(jog_down_but) == LOW) {
    Serial.println("Jogging motor down");
    jog_motor_down();
    delay(JOG_TIME);
    return;
  } else if (digitalRead(jog_up_but) == LOW) {
    Serial.println("Jogging motor up");
    jog_motor_up();
    delay(JOG_TIME);
    return;
  } else {
    // Need to handle end of jogging period
    stop_motor();
  }

  if (brightness() < BRIGHTNESS_THRESH) {
    daytime_status = BREAK;
    for (int j = 0; j < 5; j++) {
      delay_mins(1);
      if (brightness() > BRIGHTNESS_THRESH) {
        return;
      }
    }
    daytime_status = NIGHT;
    jog_motor_up();
    int start_time = millis();
    while ( (end_switch_status() == 1) & ((millis - start_time) < 2*REEL_IN_TIME)) {
      jog_motor_up();
      delay(10);
    }
    stop_motor();
    
  }

  if (brightness() > BRIGHTNESS_THRESH) {
    daytime_status = BREAK;
    for (int j = 0; j < 5; j++) {
      delay_mins(1);
      if (brightness() < BRIGHTNESS_THRESH) {
        return;
      }
    }
    daytime_status = DAY;
    jog_motor_down();
    delay(REEL_IN_TIME);
    stop_motor();
  }
*/
  
  
    /*
    status = BREAK;
    // Check for POST_SUNSET_TIME
    for (int j = 0; j < POST_SUNSET_TIME; j++) {
      delay_mins(1);
      Serial.print(j+1);
      Serial.print(" of ");
      Serial.print(POST_SUNSET_TIME);
      Serial.print(" minutes past sunset");
      if (brightness() > BRIGHTNESS_THRESH) {
        false_pos_num++;
      }
      if (false_pos_num > BRIGHTNESS_FALSE_POS_ALLOW) {
        false_pos_num = 0;
        status = DAY;
        return;
      }
    }
    */
  
  Serial.print("Brightness = ");
  Serial.println(brightness());
  Serial.print("End switch status = ");
  Serial.println(end_switch_status());

  delay(100);

}

void jog_motor_up() {
  motor.run(FORWARD);
  motor.setSpeed(100);
}

void jog_motor_down() {
  motor.run(BACKWARD);
  motor.setSpeed(100);
}

void stop_motor() {
  motor.run(BACKWARD);
  motor.setSpeed(0);
}

int brightness() {
  int i;
  long analog_val = 0;
  for (int i = 0; i < BRIGHTNESS_AV_COUNT; i++) {
    analog_val = analog_val + analogRead(light_sensor);
    delay(BRIGHTNESS_AV_TIME); 
  }
  analog_val = analog_val/BRIGHTNESS_AV_COUNT;
  //Serial.println(analog_val);
  return analog_val;
}

int end_switch_status() {
  int i;
  double analog_val = 0;
  for (int i = 0; i < END_SW_AV_COUNT; i++) {
    analog_val = analog_val + digitalRead(end_sw);
    //Serial.println(digitalRead(end_sw));
    delay(END_SW_AV_TIME); 
  }
  analog_val = analog_val/END_SW_AV_COUNT;
  if (analog_val > 0.5) {
    return 1;
  } else {
    return 0;
  }
}

void delay_mins(int mins) {
  for (int i = 0; i < mins; i++) {
    delay(1000);
  }
}
void motor_test() {
    /*
  motor.run(FORWARD);
  for (i=0; i<255; i++) {
    motor.setSpeed(i);  
    delay(3);
 }
 
  for (i=255; i!=0; i--) {
    motor.setSpeed(i);  
    delay(3);
 }
 
  motor.run(BACKWARD);
  for (i=0; i<255; i++) {
    motor.setSpeed(i);  
    delay(3);
 }
 
  for (i=255; i!=0; i--) {
    motor.setSpeed(i);  
    delay(3);
 }
 */
}
